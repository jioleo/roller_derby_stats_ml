from time import time
from sklearn.metrics import f1_score


def train_clf(clf, x_train, y_train):
    """Fits a clf to the training data."""

    # Start the clock, train the clf, then stop the clock
    start = time()
    clf.fit(x_train, y_train)
    end = time()

    # Print the results
    print("Trained model in {:.4f} seconds".format(end - start))


def predict_labels(clf, features, target):
    """Makes predictions using a fit clf based on F1 score."""

    # Start the clock, make predictions, then stop the clock
    start = time()
    y_pred = clf.predict(features)
    end = time()

    # Print and return results
    print("Made predictions in {:.4f} seconds.".format(end - start))

    return f1_score(target, y_pred, pos_label='H'), sum(target == y_pred) / float(len(y_pred))


def train_predict(clf, x_train, y_train, x_test, y_test):
    """Train and predict using a classifer based on F1 score."""
    
    # Indicate the clf and the training set size
    print("Training a {} using a training set size of {}. . .".format(clf.__class__.__name__, len(x_train)))
    
    # Train the clf
    train_clf(clf, x_train, y_train)
    
    # Print the results of prediction for both training and testing
    f1, acc = predict_labels(clf, x_train, y_train)
    print("F1 score and accuracy score for training set: {:.4f} , {:.4f}.".format(f1 , acc))
    
    f1, acc = predict_labels(clf, x_test, y_test)
    print("F1 score and accuracy score for test set: {:.4f} , {:.4f}.".format(f1 , acc))
    print('')
