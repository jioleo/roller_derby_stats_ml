from sklearn.preprocessing import scale
import pandas as pd
import src.database.data as db

"""Standardising the data."""


def scalecols(x):
    """Center to the mean and component wise scale to unit variance."""
    
    cols = [['hnbwin','vnbwin','hnbloss','vnbloss','hnbbouts','vnbbouts','haveragescored','vaveragescored','haverageconced','vaverageconced']]
    if x.shape[0] > 1:
        for col in cols:
            x[col] = scale(x[col])
    else:
        for col in cols[0]:
            min, max = db.get_min_max_col(col[1:])
            x[col] = (x[col] - min) / max
    return x


def preprocess_data(x):
    """Preprocesses the data and converts categorical variables into dummy variables.
    
    Arguments:
        x {Dataframe} -- [description]
    
    Returns:
        pandas.DataFrame -- [description]
    """
    
    x = scalecols(x)
    # Initialize new output DataFrame
    output = pd.DataFrame(index = x.index)
    # Investigate each feature column for the data
    for col, col_data in x.iteritems():
        # If data type is categorical, convert to dummy variables
        if col_data.dtype == object:
            col_data = pd.get_dummies(col_data, prefix = col)
        # Collect the revised columns
        output = output.join(col_data)
    return output


def add_missing_dummy_columns(feature, full_columns):
    missing_cols = set(full_columns) - set(feature.columns)
    for col_name in missing_cols:
        feature[col_name] = 0


def fix_columns(feature, full_columns):
    add_missing_dummy_columns(feature, full_columns)

    # # make sure we have all the columns we need
    # assert( set( full_columns ) - set( feature.columns ) == set())

    # extra_cols = set( feature.columns ) - set( full_columns )
    # if extra_cols:
    #     print("extra columns:", extra_cols)

    feature = feature[full_columns]
    return feature


def get_data_with_50_percent_home_victory(data):
    return data
