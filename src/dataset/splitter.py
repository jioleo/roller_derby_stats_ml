from sklearn.model_selection import train_test_split
import src.dataset.preprocessor as DataPreprocessor

"""Shuffle and split the dataset into training and testing set.""" 

def dataset_split(x, y):
    x = DataPreprocessor.preprocess_data(x)
    return train_test_split(x, y, 
                            test_size = 50,
                            random_state = 2,
                            stratify = y)