import pickle


DEFAULT_PATH = './trained_models/'


def save_model(model, name, feature_columns):
    pickle.dump(model, open(DEFAULT_PATH + name + '_clf.sav', 'wb'))
    pickle.dump(feature_columns, open(DEFAULT_PATH + name + '_columns.sav', 'wb'))


def load_model(name):
    model = pickle.load(open(DEFAULT_PATH + name + '_clf.sav', 'rb'))
    columns = pickle.load(open(DEFAULT_PATH + name + '_columns.sav', 'rb'))
    return model, columns