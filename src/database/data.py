import sqlite3
import pandas as pd

class DatabaseError(Exception):
    pass


def __connect():
    return sqlite3.connect('database/fts_stats.db')


def __query_dataset(query, params):
    with __connect() as db:
        data = pd.read_sql_query(query, db, params=params)
    return data.drop_duplicates(keep='first')

def get_min_max_col(col):
    data = __query_dataset(query="select min("+col+") as min, max("+col+") as max from team_data_ml", params={})
    return data['min'], data['max']

def get_all_dataset():
    """[summary]
    
    Returns:
        [type] -- return array of entry data and array of results of match
    """
    return __query_dataset(query="select * from bouts_simple_data_set", params={})


def separateFeatureTarget(data):
    # Separate into feature set and target variable
    x = data.drop(['result'],1)
    #result = (H=Home Win, V=Visitor Win)
    y = data['result']
    return x, y


def check_team_exist(searchteam):
    query = "select * from team where shortName like :search"
    result = __query_dataset(query=query, params={
            "search": "%%" + searchteam + "%%"
        })
    if len(result) != 1:
        raise DatabaseError("L'équipe '{}' n'a pas été trouvé".format(searchteam))


def get_one_feature(hometeam, visitorteam):

    check_team_exist(hometeam)
    check_team_exist(visitorteam)

    with open ('database/request/get_2_teams_data_for_predict.sql', 'r') as file:
        query = file.read().replace('\n', ' ')

    result = __query_dataset(query=query, params={
            "home_team": "%%" + hometeam + "%%",
            "visitor_team": "%%" + visitorteam + "%%"
        })
    if len(result) != 1:
        raise DatabaseError('Aucun résultat correspondant à la recherche')
    return result