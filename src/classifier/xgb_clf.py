import src.training.training as training
import xgboost as xgb

"""
    Produces a prediction model in the form of an ensemble of weak prediction models, typically decision tree
    Boosting refers to this general problem of producing a very accurate prediction rule 
    by combining rough and moderately inaccurate rules-of-thumb
"""

def train(x_train, y_train, x_test, y_test):
    clf = xgb.XGBClassifier(seed = 82)
    training.train_predict(clf, x_train, y_train, x_test, y_test)
    return clf
