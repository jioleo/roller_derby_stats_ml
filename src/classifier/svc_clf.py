import src.training.training as training
from sklearn.svm import SVC

"""
    A discriminative classifier formally defined by a separating hyperplane.
"""

def train(x_train, y_train, x_test, y_test):
    clf = SVC(random_state = 912, kernel='rbf', gamma='auto', probability=True)
    training.train_predict(clf, x_train, y_train, x_test, y_test)
    return clf
