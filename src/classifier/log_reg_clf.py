import src.training.training as training
from sklearn.linear_model import LogisticRegression

"""
    The outcome (dependent variable) has only a limited number of possible values. 
    Logistic Regression is used when response variable is categorical in nature.
"""

def train(x_train, y_train, x_test, y_test):
    clf = LogisticRegression(random_state = 42)
    training.train_predict(clf, x_train, y_train, x_test, y_test)
    return clf
