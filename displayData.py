import sqlite3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 

#Step1 Get the data from DB
def getDataSet(dbFile):
    db = sqlite3.connect(dbFile)
    query = "select * from dataset" 
    data = pd.read_sql_query(query, db)
    db.close()
    return data

data = getDataSet('../derby_data/db/fts_stats.db')
num_rows = data.shape[0]

#Step3 Create feature vectors without the victory column as x
x_raw = data.ix[:,:-1]
#encode all object type variable
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
for i in dataframe_vectors.columns:
    if dataframe_vectors[i].dtype == 'object':
        lbl = LabelEncoder()
        lbl.fit(list(dataframe_vectors[i].values))
        dataframe_vectors[i] = lbl.transform(list(dataframe_vectors[i].values))
x = dataframe_vectors.values
standard_scalar = StandardScaler()
x_std = standard_scalar.fit_transform(x)

#Step4 get victory columns as y
y = data.ix[:,-1].values
#encode victory labels
class_labels = np.unique(y)
label_encoder = LabelEncoder()
y = label_encoder.fit_transform(y)


# step 5: split the data into training set and test set
from sklearn.model_selection import train_test_split
test_percentage = 0.1
x_train, x_test, y_train, y_test = train_test_split(x_std, y, test_size = test_percentage, random_state = 0)

""" 
# t-distributed Stochastic Neighbor Embedding (t-SNE) visualization
from sklearn.manifold import TSNE
tsne = TSNE(n_components=2, random_state=0)
x_test_2d = tsne.fit_transform(x_test)

# scatter plot the sample points among 5 classes
markers=('s', 'o')
color_map = {0:'green', 1:'red'}
plt.figure()
for idx, cl in enumerate(np.unique(y_test)):
    plt.scatter(x=x_test_2d[y_test==cl,0], y=x_test_2d[y_test==cl,1], label=cl)
plt.xlabel('X in t-SNE')
plt.ylabel('Y in t-SNE')
plt.legend(loc='upper left')
plt.title('t-SNE visualization of test data')
plt.show() """

""" from pandas.plotting import scatter_matrix
scatter_matrix(data[['hgenus','vgenus','hdomain','vdomain','hnbwin','vnbwin', 'hnbloss', 'vnbloss', 'hnbbouts', 'vnbbouts', 'haveragescored', 'vaveragescored', 'haverageconced', 'vaverageconced']], figsize=(10,10))
plt.show() """
