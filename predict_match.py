import src.database.data as db
import src.dataset.preprocessor as DataPreprocessor
import src.files_manager.model_file as ModelManager


MODEL_PATH = './trained_models/svc_clf.sav'


def get_feature_match_to_predict(hometeam, visitorteam):
    data = db.get_one_feature(hometeam, visitorteam)
    return DataPreprocessor.preprocess_data(data)


def build_model_and_feature(modelname, hometeam, visitorteam):
    model, columns = ModelManager.load_model(modelname)
    feature = get_feature_match_to_predict(hometeam, visitorteam)
    feature = DataPreprocessor.fix_columns(feature, columns)
    return model, feature


def predict_match_result_with_proba(modelname, hometeam, visitorteam):
    model, feature = build_model_and_feature(modelname, hometeam, visitorteam)
    return model.predict_proba(feature)[0]*100


def predict_match_result(modelname, hometeam, visitorteam):
    model, feature = build_model_and_feature(modelname, hometeam, visitorteam)
    if model.predict(feature) == 'H':
        winner = hometeam
    else:
        winner = visitorteam
    return winner


if __name__ == "__main__":
    hometeam = "gatekeepers"
    visitorteam = "bonhommes"
    try:
        model = 'xgb'
        winner = predict_match_result(model, hometeam, visitorteam)
        print("\n {} VS {}".format(hometeam, visitorteam))
        proba = predict_match_result_with_proba(model, hometeam, visitorteam)
        print(" {} ({:.0f}% chance de victoire) {} ({:.0f}% chance de victoire)\n".format(hometeam, proba[0], visitorteam, proba[1]))
    except Exception as e:
        print(e.args[0])
    