
import src.database.data as db
import src.dataset.splitter as Splitter
import src.dataset.preprocessor as DataPreprocessor
import src.classifier.log_reg_clf as lg
import src.classifier.svc_clf as svc
import src.classifier.xgb_clf as xgb
import src.files_manager.model_file as ModelManager


def execute():
    """ Prepare Data """
    
    data = db.get_all_dataset()

    #data = DataPreprocessor.get_data_with_50_percent_home_victory(data)

    x, y = db.separateFeatureTarget(data)

    x = DataPreprocessor.scalecols(x)
    x = DataPreprocessor.preprocess_data(x)
    x_train, x_test, y_train, y_test = Splitter.dataset_split(x,y)

    feature_columns = x.columns

    """ Train Classifiers """

    lg_clf = lg.train(x_train, y_train, x_test, y_test)
    ModelManager.save_model(lg_clf, 'lg', feature_columns)

    xgb_clf = xgb.train(x_train, y_train, x_test, y_test)
    ModelManager.save_model(xgb_clf, 'xgb', feature_columns)

    svc_clf = svc.train(x_train, y_train, x_test, y_test)
    ModelManager.save_model(svc_clf, 'svc', feature_columns)

if __name__ == "__main__":
    execute()        

