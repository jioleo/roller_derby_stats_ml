-- Create a view for nb win & loss & averageScore / team
CREATE VIEW team_stats
AS 
select wins.id, nbwin, nbloss, (nbwin + nbloss) as nbbouts, 
    averageScored, 
    averageConced  
from (
    select 
        id, 
        sum(nbwin) as nbwin 
    from (
        select 
            h.id,
            count(*) as nbwin 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id
        group by h.id, (bout.homeScore - bout.visitorScore) > 0
        having (bout.homeScore - bout.visitorScore) > 0

        union 

        select 
            v.id,
            count(*) as nbwin 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id
        group by v.id, (bout.visitorScore - bout.homeScore) > 0
        having (bout.visitorScore - bout.homeScore) > 0
    ) group by id
) wins
join (
    select 
        id, 
        sum(nbloss) as nbloss 
    from (
        select 
            h.id,
            count(*) as nbloss 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id
        group by h.id, (bout.homeScore - bout.visitorScore) < 0
        having (bout.homeScore - bout.visitorScore) < 0

        union 

        select 
            v.id,
            count(*) as nbloss 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id
        group by v.id, (bout.visitorScore - bout.homeScore) < 0
        having (bout.visitorScore - bout.homeScore) < 0
    ) group by id
) losses on wins.id = losses.id
join (
    select id, sum(score) / count(score) as averageScored  from (
        select 
        h.id,
        bout.homeScore as score 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id

        union 

        select 
        v.id,
        bout.visitorScore as score 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id
    ) group by id
) averageScored on wins.id = averageScored.id
join (
    select id, sum(score) / count(score) as averageConced  from (
        select 
        h.id,
        bout.visitorScore as score 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id

        union 

        select 
        v.id,
        bout.homeScore as score 
        from bout
        join team h on bout.homeTeam = h.id
        join team v on bout.visitorTeam = v.id
    ) group by id
) averageConced on wins.id = averageConced.id
;
