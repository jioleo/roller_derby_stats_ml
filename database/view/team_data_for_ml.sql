DROP VIEW team_data_ml;
CREATE VIEW team_data_ml
AS 
select 
    t.id, 
    s.nbwin, 
    s.nbloss,
    s.nbbouts, 
    s.averageScored, 
    s.averageConced
    -- t.genus, 
    -- t.domain,
    -- case (t.parentLeague == '')
    -- when true then 0
    -- when false then 1
    -- end as hasParentLeague
from team t
join team_stats s on t.id = s.id
;
