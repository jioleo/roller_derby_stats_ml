DROP VIEW bouts_simple_data_set;
CREATE VIEW bouts_simple_data_set 
AS
select 
    ht.nbwin as hnbwin, 
    vt.nbwin as vnbwin, 
    ht.nbloss as hnbloss, 
    vt.nbloss as vnbloss, 
    ht.nbbouts as hnbbouts, 
    vt.nbbouts as vnbbouts, 
    ht.averageScored as haveragescored, 
    vt.averageScored as vaveragescored, 
    ht.averageConced as haverageconced, 
    vt.averageConced as vaverageconced, 
    -- ht.genus as hgenus, 
    -- vt.genus as vgenus, 
    -- ht.domain as hdomain, 
    -- vt.domain as vdomain, 
    -- ht.hasParentLeague as hhasparent,
    -- vt.hasParentLeague as vhasparent, 
    CASE (b.homeScore - b.visitorScore) > 0
        WHEN true THEN 'H'
        WHEN false THEN 'V'
    END AS result 
from bout b 
join team_stats ht on b.homeTeam = ht.id 
join team_stats vt on b.visitorTeam = vt.id
;
