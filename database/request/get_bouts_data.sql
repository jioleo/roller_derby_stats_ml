select 
bout.date,
h.id as hid, 
h.name as hname, 
h.genus as hgenus, 
h.teamType as hteamtype, 
h.domain as hdomain,
bout.homeScore as hscore,
CASE (bout.homeScore - bout.visitorScore) < 0
    WHEN true THEN 'H' 
    WHEN false THEN 'V'
END as win, 
bout.visitorScore as vscore,
v.id as vid, 
v.name as vname, 
v.genus as vgenus, 
v.teamType as vteamtype, 
v.domain as vdomain
from bout
join team h on bout.homeTeam = h.id
join team v on bout.visitorTeam = v.id
;
