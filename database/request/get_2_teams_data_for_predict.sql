select 
    hnbwin, 
    vnbwin, 
    hnbloss, 
    vnbloss, 
    hnbbouts, 
    vnbbouts, 
    haveragescored, 
    vaveragescored, 
    haverageconced, 
    vaverageconced
from
(
    select 
        ht.nbwin as hnbwin, 
        ht.nbloss as hnbloss, 
        ht.nbbouts as hnbbouts, 
        ht.averageScored as haveragescored, 
        ht.averageConced as haverageconced 
    from team t 
    join team_data_ml ht on ht.id = t.id 
    where t.shortName like :home_team 
)
join (
    select 
        vt.nbwin as vnbwin, 
        vt.nbloss as vnbloss, 
        vt.nbbouts as vnbbouts, 
        vt.averageScored as vaveragescored, 
        vt.averageConced as vaverageconced 
    from team t 
    join team_data_ml vt on vt.id = t.id 
    where t.shortName like :visitor_team
)