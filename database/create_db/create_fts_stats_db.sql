-- Down
DROP TABLE bout;
DROP TABLE tournament;
DROP TABLE team;

-- Up
CREATE TABLE team (
    id	            INTEGER PRIMARY KEY,
    name	        TEXT NOT NULL,
    shortName	    TEXT,
    abbreviation	TEXT,
    teamType	    TEXT NOT NULL,
    location	    TEXT,
    website	        TEXT,
    parentLeague	INTEGER,
    establishedDate	TEXT,
    disbandedDate	TEXT,
    genus	        TEXT NOT NULL,
    domain	        TEXT NOT NULL,
    CONSTRAINT team_team_fk_parentleague_id FOREIGN KEY (parentLeague) REFERENCES team(id)
);

CREATE TABLE tournament (
    id              INTEGER PRIMARY KEY,
    name            TEXT NOT NULL,
    type            TEXT NOT NULL,
    location        TEXT,
    startDate       NUMERIC NOT NULL,
    endDate         NUMERIC NOT NULL,
    website         TEXT,
    hostingTeams    TEXT
);

CREATE TABLE bout (
    id	            INTEGER PRIMARY KEY,
    date            TEXT,
    time            TEXT,
    tournament      INTEGER,
    homeTeam        INTEGER NOT NULL,
    homeScore       NUMERIC NOT NULL,
    visitorTeam	    INTEGER NOT NULL,
    visitorScore    NUMERIC NOT NULL,
    CONSTRAINT bout_team_fk_hometeam_id FOREIGN KEY (homeTeam) REFERENCES team(id),
    CONSTRAINT bout_team_fk_visitorteam_id FOREIGN KEY (visitorTeam) REFERENCES team(id),
    CONSTRAINT bout_tournament_fk_tournament_id FOREIGN KEY (tournament) REFERENCES tournament(id)
);
